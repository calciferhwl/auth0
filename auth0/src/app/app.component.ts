import { Component } from '@angular/core';
import { AuthService } from './services/auth/auth.component';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private auth : AuthService){

  }
}
