import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './auth/auth.component';
@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  // ng g guard services/auth --skipTests
  // inyectar el servicio de authservice


  constructor(private auth: AuthService){

  }


  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.auth.isAuthenticated$;
  }
  
}
